from __future__ import absolute_import

import datetime
import os
import tempfile
from unittest import TestCase, main

import dateutil.tz

from .migrate_database import do_migrations
from .models import Repository


class IssueRepositoryTest(TestCase):
    def setUp(self):
        self.db_file = tempfile.mktemp()
        repo = Repository(self.db_file)
        repo.migrate_database()
        self.repo_conn = repo.open()
        self.instance = self.repo_conn.issues

        # issue using qword params in sqlite3 in double digit chars.
        for _ in range(0, 10):
            self.instance.create_issue("Test Issue", "Test Issue Description")

    def tearDown(self):
        self.repo_conn.close()
        os.remove(self.db_file)

    def test_create_and_fetch(self):
        issue_id = self.instance.create_issue("Test Issue", "Test Issue Description")
        issue = self.instance.fetch_issue(issue_id)
        self.assertEqual(issue.id, issue_id)
        self.assertEqual(issue.title, "Test Issue")
        self.assertEqual(issue.description, "Test Issue Description")
        self.assertTrue(isinstance(issue.opened, datetime.datetime))
        self.assertEqual(issue.closed, None)

        # ensure dangerous strings are properly handled
        # in order to avoid nasty sql injections.
        issue_id = self.instance.create_issue(
            "this' -- should be fine", "test description with' -- nasty chars"
        )
        issue = self.instance.fetch_issue(issue_id)
        self.assertEqual(issue.id, issue_id)
        self.assertEqual(issue.title, "this' -- should be fine")
        self.assertEqual(issue.description, "test description with' -- nasty chars")
        self.assertTrue(isinstance(issue.opened, datetime.datetime))
        self.assertEqual(issue.closed, None)

    def test_create_and_list(self):
        issue_id_1 = self.instance.create_issue("Test Issue", "Test Issue Description")
        issue_id_2 = self.instance.create_issue(
            "Test 'Issue' -- 2", "Test Issue Description 2"
        )
        issues = self.instance.list_issues()
        # created 10 in setup
        self.assertEqual(issues[10].id, issue_id_1)
        self.assertEqual(issues[10].title, "Test Issue")
        self.assertEqual(issues[10].description, "Test Issue Description")
        self.assertEqual(issues[11].id, issue_id_2)
        self.assertEqual(issues[11].title, "Test 'Issue' -- 2")
        self.assertEqual(issues[11].description, "Test Issue Description 2")

    def test_update(self):
        issue_id = self.instance.create_issue(
            "Test -- 'Issue'", "Test Issue Description"
        )
        self.instance.update_issue(
            issue_id,
            title="New Title",
            description="New Description",
            closed=datetime.datetime(2020, 1, 1),
        )
        issue = self.instance.fetch_issue(issue_id)
        self.assertEqual(issue.id, issue_id)
        self.assertEqual(issue.title, "New Title")
        self.assertEqual(issue.description, "New Description")
        self.assertEqual(
            issue.closed,
            datetime.datetime(2020, 1, 1).replace(tzinfo=dateutil.tz.gettz()),
        )


if __name__ == "__main__":
    main()
